﻿namespace EFViews.CentralContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CentralThings",
                c => new
                    {
                        CentralThingId = c.Int(nullable: false, identity: true),
                        SharedParentId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CentralThingId)
                .ForeignKey("dbo.SharedParents", t => t.SharedParentId, cascadeDelete: true)
                .Index(t => t.SharedParentId);
            
            CreateTable(
                "dbo.SharedParents",
                c => new
                    {
                        SharedParentId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SharedParentId);
            
            CreateTable(
                "dbo.SharedChilds",
                c => new
                    {
                        SharedChildId = c.Int(nullable: false, identity: true),
                        SharedParentId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SharedChildId)
                .ForeignKey("dbo.SharedParents", t => t.SharedParentId, cascadeDelete: true)
                .Index(t => t.SharedParentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SharedChilds", "SharedParentId", "dbo.SharedParents");
            DropForeignKey("dbo.CentralThings", "SharedParentId", "dbo.SharedParents");
            DropIndex("dbo.SharedChilds", new[] { "SharedParentId" });
            DropIndex("dbo.CentralThings", new[] { "SharedParentId" });
            DropTable("dbo.SharedChilds");
            DropTable("dbo.SharedParents");
            DropTable("dbo.CentralThings");
        }
    }
}
