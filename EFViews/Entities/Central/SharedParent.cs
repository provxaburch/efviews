﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFViews.Entities.Central
{
    public class SharedParent
    {
        public virtual int SharedParentId { get; set; }

        public virtual string Name { get; set; }

        // Navigation

        // excuse the horrible pluralization, I just want everything to hook up via conventions
        public virtual ICollection<SharedChild> SharedChildren { get; set; }

        public virtual ICollection<CentralThing> CentralThings { get; set; }
    }
}
