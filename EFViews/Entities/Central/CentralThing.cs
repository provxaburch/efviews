﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFViews.Entities.Central
{
    public class CentralThing
    {
        public virtual int CentralThingId { get; set; }

        public virtual int SharedParentId { get; set; }

        public virtual string Name { get; set; }

        public virtual SharedParent SharedParent { get; set; }
    }
}
