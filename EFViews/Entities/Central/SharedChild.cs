﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFViews.Entities.Central
{
    public class SharedChild
    {
        public virtual int SharedChildId { get; set; }

        public virtual int SharedParentId { get; set; }

        public virtual string Name { get; set; }

        // Navigation
        public virtual SharedParent SharedParent { get; set; }
    }
}
