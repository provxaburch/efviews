﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFViews.Entities.Central;

namespace EFViews.Entities.Tenant
{
    public class TenantThing
    {
        public virtual int TenantThingId { get; set; }

        public virtual int SharedParentId { get; set; }

        public virtual string Name { get; set; }

        public virtual SharedParent SharedParent { get; set; }
    }
}
