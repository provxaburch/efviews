﻿namespace EFViews.TenantContextMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TenantThings",
                c => new
                    {
                        TenantThingId = c.Int(nullable: false, identity: true),
                        SharedParentId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TenantThingId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TenantThings");
        }
    }
}
