﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFViews.Entities.Central;
using EFViews.Entities.Tenant;

namespace EFViews.Contexts
{
    public class TenantContext : DbContextBase
    {
        static TenantContext()
        {
            Database.SetInitializer<TenantContext>(null);
        }

        public TenantContext()
            : base("TenantContext")
        {
        }

        public DbSet<SharedParent> SharedParents { get; set; }

        public DbSet<SharedChild> SharedChildren { get; set; }

        public DbSet<TenantThing> TenantThings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Ignore central entities during migration
            if (IsMigrating)
            {
                modelBuilder.Ignore<SharedParent>();
                modelBuilder.Ignore<SharedChild>();
            }
        }
    }
}
