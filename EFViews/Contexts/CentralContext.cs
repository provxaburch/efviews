﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFViews.Entities.Central;

namespace EFViews.Contexts
{
    public class CentralContext : DbContextBase
    {
        static CentralContext()
        {
            Database.SetInitializer<CentralContext>(null);
        }

        public CentralContext()
            : base("CentralContext")
        {
        }

        public DbSet<SharedParent> SharedParents { get; set; }

        // excuse the horrible pluralization, I just want everything to hook up via conventions
        public DbSet<SharedChild> SharedChildren { get; set; }

        public DbSet<CentralThing> CentralThings { get; set; }
    }
}
