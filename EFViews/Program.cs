﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFViews.Contexts;
using EFViews.Entities.Central;

namespace EFViews
{
    class Program
    {
        static void Main(string[] args)
        {
            DbContextBase.IsMigrating = false;

            int parentId;
            using (var ctx = new CentralContext())
            {
                var parent = ctx.SharedParents.Add(ctx.SharedParents.Create());
                parent.Name = "First Parent";
                
                var child = ctx.SharedChildren.Add(ctx.SharedChildren.Create());
                child.SharedParent = parent;
                child.Name = "First Child";

                var centralThing = ctx.CentralThings.Add(ctx.CentralThings.Create());
                centralThing.SharedParent = parent;
                centralThing.Name = "First Thing";

                ctx.SaveChanges();
                parentId = parent.SharedParentId;
            }

            using (var ctx = new TenantContext())
            {
                var tenantThing = ctx.TenantThings.Add(ctx.TenantThings.Create());
                tenantThing.SharedParentId = parentId;
                tenantThing.Name = "First Thing";
                ctx.SaveChanges();

                // let's see if the central entities are available for demand load now
                Console.WriteLine($"Nav TenantThing to Shared Parent: {tenantThing.SharedParent.Name}");

                foreach (var parent in ctx.SharedParents.ToArray())
                {
                    Console.WriteLine($"Parent \"{parent.Name}\"");
                    foreach (var child in parent.SharedChildren.ToArray())
                    {
                        Console.WriteLine($"  Child \"{child.Name}\"");
                    }

                    // Intersting that this works, given that there's no CentralThings DbSet on the tenant context.
                    if (parent.CentralThings != null)
                    {
                        foreach (var thing in parent.CentralThings.ToArray())
                        {
                            Console.WriteLine($"  Thing \"{thing.Name}\"");
                        }
                    }
                }
            }

        }
    }
}
